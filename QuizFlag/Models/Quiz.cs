﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlag.Models
{
    public class Quiz
    {
        public int Id { get; set; }
        public Jogador Jogador { get; set; }
        public Bandeira Bandeira { get; set; }
        public int? Ordem { get; set; }
        public Bandeira Resposta { get; set; }
    }
}