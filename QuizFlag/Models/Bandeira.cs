﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlag.Models
{
    public class Bandeira
    {
        public int Id { get; set; }
        public string NomePais { get; set; }
        public string NomeArquivo { get; set; }
    }
}