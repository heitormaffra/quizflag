namespace QuizFlag.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class QuizFlagContext : DbContext
    {
        // Your context has been configured to use a 'QuizFlagContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'QuizFlag.Models.QuizFlagContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'QuizFlagContext' 
        // connection string in the application configuration file.
        public QuizFlagContext()
            : base("name=QuizFlagContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Bandeira> Bandeira { get; set; }
        public virtual DbSet<Jogador> Jogador { get; set; }
        public virtual DbSet<Quiz> Quiz { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}