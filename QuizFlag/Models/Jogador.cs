﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlag.Models
{
    public class Jogador
    {
        public int Id { get; set; }
        public string NomeJogador { get; set; }
    }
}