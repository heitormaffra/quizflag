﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuizFlag.Models;

namespace QuizFlag.ViewModel
{
    public class QuizVM
    {
        public int Id { get; set; }
        public Jogador Jogador { get; set; }
        public Bandeira Bandeira { get; set; }
        public List<Bandeira> Opcoes { get; set; }
        public int? Ordem { get; set; }
        public Bandeira Resposta { get; set; }
    }
}