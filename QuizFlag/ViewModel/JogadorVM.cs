﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlag.ViewModel
{
    public class JogadorVM
    {
        public int Id { get; set; }
        public string NomeJogador { get; set; }
    }
}