﻿using System.Web.Mvc;
using QuizFlag.ViewModel;
using QuizFlag.Repository;
using System.Collections.Generic;

namespace QuizFlag.Controllers
{
    public class QuizController : Controller
    {
        private QuizRepository quizRep = new QuizRepository();
        // GET: Quiz
        public ActionResult Index(int idJogador, int? ordem)
        {
            if (!ordem.HasValue)
            {
                ordem = 1;
                QuizVM quiz = quizRep.GerarNovoQuiz(idJogador, ordem);

                return View(quiz);
            } else
            {
                if (ordem > 10)
                {
                    return RedirectToAction("Resultado", new { idJogador = idJogador });
                }
                else
                {
                    
                    QuizVM quiz = quizRep.GerarNovoQuiz(idJogador, ordem);

                    return View(quiz);
                }                
            }           
            
        }

        public ActionResult ProximaQuestao(int idJogador, int ordem, int idBandeira, int opcao)
        {
            QuizVM quizSalvo = quizRep.SalvarResposta(idJogador, ordem, idBandeira, opcao);
            int? novaOrdem = ++ordem;
            return RedirectToAction("Index", "Quiz", new { idJogador = quizSalvo.Jogador.Id, ordem = novaOrdem });
        }

        public ActionResult Resultado(int idJogador)
        {
            List<QuizVM> lista = quizRep.ListarResultado(idJogador);
            
            return View(lista);
        }
    }
}