﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuizFlag.Repository;
using QuizFlag.ViewModel;

namespace QuizFlag.Controllers
{
    public class HomeController : Controller
    {
        private JogadorRepository jogadorRep = new JogadorRepository();

        public ActionResult Index()
        {
            List<JogadorVM> jogadores = jogadorRep.ListarJogadores();

            return View(jogadores);
        }

        [HttpPost]
        public ActionResult Index(string NomeJogador)
        {
            int id = jogadorRep.Adicionar(NomeJogador);

            return RedirectToAction("Index", "Quiz", new { idJogador = id });
        }

    }
}