﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuizFlag.Models;
using QuizFlag.ViewModel;

namespace QuizFlag.Repository
{
    public class JogadorRepository
    {
        private QuizFlagContext dbContext = new QuizFlagContext();

        public int Adicionar(string nomeJogador)
        {
            Jogador jogador = new Jogador();
            jogador.NomeJogador = nomeJogador;

            dbContext.Jogador.Add(jogador);
            dbContext.SaveChanges();
            dbContext.Entry(jogador).GetDatabaseValues();

            return jogador.Id;
        }

        public List<JogadorVM> ListarJogadores()
        {

            var jogadores = from j in dbContext.Jogador
                            select new JogadorVM()
                            {
                                Id = j.Id,
                                NomeJogador = j.NomeJogador
                            };

            return jogadores.ToList();
        }

    }
}