﻿using QuizFlag.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuizFlag.ViewModel;
using QuizFlag.Extensions;

namespace QuizFlag.Repository
{
    public class QuizRepository
    {
        private QuizFlagContext dbContext = new QuizFlagContext();

        /// <summary>
        /// Gera um novo quiz randômico com 10 perguntas.
        /// </summary>
        /// <returns></returns>
        public QuizVM GerarNovoQuiz(int idJogador, int? ordem)
        {
            List<Quiz> perguntasRespondidas = new List<Quiz>();

            perguntasRespondidas = dbContext.Quiz.Where(j => j.Jogador.Id == idJogador).ToList();

            List<Bandeira> bandeiras = dbContext.Bandeira.ToList();
            foreach(var p in perguntasRespondidas)
            {
                bandeiras.Remove(p.Bandeira);
            }
                        
            bandeiras.Shuffle();


            Quiz quiz = new Quiz();

            quiz.Ordem = ordem.Value;

            quiz.Bandeira = bandeiras.First();

            bandeiras.Shuffle();

            quiz.Jogador = dbContext.Jogador.Find(idJogador);

            List<Bandeira> opcoesUnicas = bandeiras;

            opcoesUnicas.Remove(quiz.Bandeira);

            List<Bandeira> opcoes = opcoesUnicas.Take(3).ToList();
            
            //opcoes.AddRange();
            opcoes.Add(quiz.Bandeira);
            opcoes.Shuffle();

            QuizVM quizVm = new QuizVM()
            {
                Bandeira = quiz.Bandeira,
                Id = quiz.Id,
                Jogador = quiz.Jogador,
                Opcoes = opcoes,
                Ordem = quiz.Ordem
            };

            return quizVm;
        }

        public QuizVM SalvarResposta(int idJogador, int ordem, int idBandeira, int opcao)
        {

            Jogador jogador = dbContext.Jogador.Find(idJogador);
            Bandeira resposta = dbContext.Bandeira.Find(opcao);
            Bandeira bandeira = dbContext.Bandeira.Find(idBandeira);

            List<Quiz> perguntas = dbContext.Quiz.Where(j => j.Jogador.Equals(jogador)).ToList();

            if(perguntas.Count == 10)
            {
                dbContext.Quiz.RemoveRange(perguntas);
                dbContext.SaveChanges();
            }

            Quiz quizModel = new Quiz()
            {
                Bandeira = bandeira,
                Jogador = jogador,
                Ordem = ordem,
                Resposta = resposta
            };

            dbContext.Quiz.Add(quizModel);
            dbContext.SaveChanges();

            dbContext.Entry(quizModel).GetDatabaseValues();

            QuizVM quizVm = new QuizVM()
            {
                Bandeira = quizModel.Bandeira,
                Id = quizModel.Id,
                Jogador = quizModel.Jogador,
                Ordem = quizModel.Ordem,
                Resposta = quizModel.Resposta
            };

            return quizVm;
        }

        public List<QuizVM> ListarResultado(int idJogador)
        {
            var quiz = from q in dbContext.Quiz
                       where q.Jogador.Id == idJogador
                       select new QuizVM()
                       {
                           Bandeira = q.Bandeira,
                           Id = q.Id,
                           Jogador = q.Jogador,
                           Resposta = q.Resposta
                       };
            return quiz.ToList();
        }
    }
    
}
